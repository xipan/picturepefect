﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PicturePerfect.Models
{
    public class PhotoDef
    {
        public string PathOfPhoto { get; set; }

        public int WidthOfPhoto { get; set; }

        public int HeightOfPhoto { get; set; }
        public ImageSource StreamOfPhoto { get; set; }
    }

    public class DominantColor
    {
        public Color PixelOfColor { get; set; }
        public int CountOfColor { get; set; }
    }
}
