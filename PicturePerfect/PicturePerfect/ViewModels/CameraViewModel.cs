﻿using PicturePerfect.Models;
using Plugin.Media;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PicturePerfect.ViewModels
{
    public class CameraViewModel : BaseViewModel
    {
        PhotoDef _currentPhoto;

        private SkiaSharp.SKColor[,] allColor;

        private Dictionary<SkiaSharp.SKColor, int> dic;

        ImageSource imageSourceOfPhoto = null;
        public ImageSource ImageSourceOfPhoto
        {
            get { return imageSourceOfPhoto; }
            set { SetProperty(ref imageSourceOfPhoto, value); }
        }

        public Command TakePicture { get; set; }

        public List<DominantColor> DominantColorItemSource { get; set; }

        public CameraViewModel()
        {
            Title = "Photo";
            _currentPhoto = new PhotoDef();
            TakePicture = new Command(() => ExecuteTakePictureCommad());

            DominantColorItemSource = new List<DominantColor>();            


        }

        async void ExecuteTakePictureCommad()
        {
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                await Application.Current.MainPage.DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
                return;
            }
            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                Directory = "Sample",
                Name = "test.jpg"
            });
            if (file == null)
                return;

            this._currentPhoto.PathOfPhoto = file.Path.ToString();
            await Application.Current.MainPage.DisplayAlert("File Location", file.Path, "OK");

            Stream fileStream = File.OpenRead(file.Path); // open a stream to an image file
            if (fileStream != null)
            {
                SKBitmap libraryBitmap = SKBitmap.Decode(fileStream);
                allColor = new SkiaSharp.SKColor[libraryBitmap.Height, libraryBitmap.Width];

                int startRow1 = 0;
                int endRow1 = (int)Math.Floor((double)libraryBitmap.Height / 2);
                int startRow2 = endRow1 +1;
                int endRow2 = libraryBitmap.Height - 1;


                await Task.Run(async () =>
                {
                    ReloadPixel(libraryBitmap, startRow1, endRow1);
                });

                await Task.Run(async () =>
                {
                    ReloadPixel(libraryBitmap, startRow2, endRow2);
                });
                ConvertFromArrayToDictionary(libraryBitmap.Height, libraryBitmap.Width);
                //ReloadPixel(libraryBitmap, 0, libraryBitmap.Height - 1);
            }

            this.ImageSourceOfPhoto = ImageSource.FromStream(() =>
            {
                var stream1 = file.GetStream();
                file.Dispose();
                return stream1;
            });
            this._currentPhoto.StreamOfPhoto = this.ImageSourceOfPhoto;

            this.RefreshProperty();
        }

        private void ReloadPixel(SKBitmap libraryBitmap, int startRow, int endRow)
        {
            for (int h = startRow; h <= endRow; h++)
                for (int w = 0; w < libraryBitmap.Width; w++)
                {
                    allColor[h, w] = libraryBitmap.GetPixel(w, h);
                }
        }

        private void ConvertFromArrayToDictionary(int photoHeight, int photoWidht)
        {
            dic = new Dictionary<SkiaSharp.SKColor, int>();

            for (int h = 0; h < photoHeight; h++)
            {
                for (int w = 0; w < photoWidht; w++)
                {

                    SkiaSharp.SKColor itemColor = allColor[h, w];

                    if (dic.ContainsKey(itemColor))
                    {
                        dic[itemColor] = dic[itemColor] + 1;
                    }
                    else
                    {
                        dic.Add(itemColor, 1);
                    }

                }
            }
            
            var sortedDict = dic.OrderByDescending(x => x.Value).Take(5).ToList();
            DominantColorItemSource = new List<DominantColor>();
            
            for (int count = 0; count < sortedDict.Count; count++)
            {
                var element = sortedDict.ElementAt(count);                
                var Key = element.Key;
                var Value = element.Value;                
                DominantColorItemSource.Add(new DominantColor() { PixelOfColor = Color.FromRgb(element.Key.Red, element.Key.Green, element.Key.Blue), CountOfColor = element.Value });
            }
        }

        private void RefreshProperty()
        {
            OnPropertyChanged("ImageSourceOfPhoto");
            OnPropertyChanged("DominantColorItemSource");
        }

    }
}
