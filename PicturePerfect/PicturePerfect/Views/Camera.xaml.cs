﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PicturePerfect.ViewModels;

namespace PicturePerfect.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Camera : ContentPage
    {
        CameraViewModel viewModel;

        public Camera()
        {
            BindingContext = viewModel = new CameraViewModel();

            InitializeComponent();
        }
    }
}