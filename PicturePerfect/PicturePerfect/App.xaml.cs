﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PicturePerfect.Views;

namespace PicturePerfect
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            MainPage = new Camera();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
